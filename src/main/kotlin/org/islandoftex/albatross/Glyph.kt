// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.albatross

import java.text.BreakIterator
import kotlin.streams.toList

/**
 * Adds an iterator to [BreakIterator], yielding
 * an [IntRange] based on the type of break.
 * @return an [IntRange] based on the type of
 * break provided by the concrete class.
 */
operator fun BreakIterator.iterator(): Iterator<IntRange> = iterator {
    var start = first()
    while (true) {
        val end = next()
        if (end == BreakIterator.DONE) {
            break
        }
        yield(start until end)
        start = end
    }
}

data class Glyph(
    val codepoints: List<String>,
    val string: String
) {

    companion object {

        /**
         * Gets a [Glyph] based on the provided
         * string representation. This implementation
         * attempts to extract graphemes that compose
         * a certain glyph (or return null otherwise).
         * @return the corresponding [Glyph] or null.
         */
        fun fromStringOrNull(text: String) =
            getGraphemes(text).takeIf { it.size == 1 }?.let { Glyph(it[0], text) }

        /**
         * Gets a [Glyph] based on the provided
         * hexadecimal string code point.
         * @return the corresponding [Glyph].
         */
        fun fromCodePointOrNull(codepoint: String) = try {
            Glyph(listOf(codepoint.uppercase()), Character.toChars(codepoint.toInt(Settings.hexRadix)).concatToString())
        } catch (_: NumberFormatException) {
            // The provided codepoint does not fit into the integer
            // range and is not considered valid input.
            null
        }

        /**
         * Gets a list of graphemes from the given
         * string based on the character break iterator.
         * @return a list of lists of [String] denoting
         * the graphemes based on the break iterator.
         */
        private fun getGraphemes(text: String): List<List<String>> {
            val iterator = BreakIterator.getCharacterInstance().also { it.setText(text) }
            return buildList {
                for (i in iterator) {
                    add(text.substring(i).codePoints().toList().map { x -> x.toString(Settings.hexRadix).uppercase() })
                }
            }
        }
    }
}
