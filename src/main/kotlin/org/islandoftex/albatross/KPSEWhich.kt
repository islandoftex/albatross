// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.albatross

import java.io.IOException
import java.nio.file.Paths
import kotlin.io.path.div

object KPSEWhich {

    @JvmStatic
    val version: String
        @Throws(IOException::class)
        get() = CommandLine.getCommandOutput(listOf("kpsewhich", "--version"))[0]

    @JvmStatic
    val isAvailable: Boolean by lazy {
        runCatching { version.isNotBlank() }.getOrElse { false }
    }

    /**
     * Gets the [Paths] of the system TeX fonts.
     * @return the [Paths] of the system TeX fonts.
     * @throws [IOException] in case
     * the command is not found.
     */
    @Throws(IOException::class)
    fun getSystemTeXFontsPath() =
        Paths.get(CommandLine.getCommandOutput(listOf("kpsewhich", "--var-value=TEXMFDIST"))[0]) / "fonts"

    /**
     * Gets the [Paths] of the local TeX fonts.
     * @return the [Paths] of the local TeX fonts.
     * @throws [IOException] in case
     * the command is not found.
     */
    @Throws(IOException::class)
    fun getLocalTeXFontsPath() =
        Paths.get(CommandLine.getCommandOutput(listOf("kpsewhich", "--var-value=TEXMFLOCAL"))[0]) / "fonts"
}
