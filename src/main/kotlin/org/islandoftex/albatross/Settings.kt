// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.albatross

import com.github.ajalt.mordant.rendering.TextColors
import com.github.ajalt.mordant.rendering.TextStyle

object Settings {
    const val fontNameColumnFullWidthPercentage = 100
    const val fontNameColumnWidthPercentage = 40
    const val fontStylesColumnWidthPercentage = 60
    const val noEntriesColumnWidthPercentage = 100
    const val notFoundColumnWidthPercentage = 100
    const val detailsKeyColumnWidthPercentage = 12
    const val detailsValueColumnWidthPercentage = 88
    const val detailsPathsColumnWidthPercentage = 100

    val cliTextStyle = TextColors.Companion.rgb("b6a0ff")
    val cliPathStyle = TextColors.Companion.rgb("a8a8a8")
    val cliTitleStyle = TextColors.Companion.rgb("dbbe5f") + TextStyle(bold = true)
    val cliHeaderStyle = TextColors.Companion.rgb("26a269") + TextStyle(bold = true)
    val cliProgressTextStyle = TextColors.Companion.rgb("dbbe5f")
    val cliProgressSpinnerStyle = TextColors.Companion.rgb("dbbe5f")

    val borderColour = TextColors.Companion.rgb("00d1d0")

    val fontExtensions = setOf("otf", "pfa", "woff", "woff2", "ttf", "ttc", "pfb", "pfm")
    const val formatFontConfig = "family=%{family}\\nfamilylang=%{familylang}\\nstyle=%{style}\\n" +
        "stylelang=%{stylelang}\\nfullname=%{fullname}\\nfullnamelang=%{fullnamelang}\\nslant=%{slant}\\n" +
        "weight=%{weight}\\nsize=%{size}\\nwidth=%{width}\\naspect=%{aspect}\\npixelsize=%{pixelsize}\\n" +
        "spacing=%{spacing}\\nfoundry=%{foundry}\\nantialias=%{antialias}\\nhinting=%{hinting}\\n" +
        "hintstyle=%{hintstyle}\\nverticallayout=%{verticallayout}\\nautohint=%{autohint}\\n" +
        "globaladvance=%{globaladvance}\\nfile=%{file}\\nindex=%{index}\\nftface=%{ftface}\\n" +
        "rasterizer=%{rasterizer}\\noutline=%{outline}\\nscalable=%{scalable}\\ncolor=%{color}\\n" +
        "scale=%{scale}\\ndpi=%{dpi}\\nrgba=%{rgba}\\nlcdfilter=%{lcdfilter}\\nminspace=%{minspace}\\n" +
        "charset=%{charset}\\nlang=%{lang}\\nfontversion=%{fontversion}\\ncapability=%{capability}\\n" +
        "fontformat=%{fontformat}\\nembolden=%{embolden}\\nembeddedbitmap=%{embeddedbitmap}\\n" +
        "decorative=%{decorative}\\nfontfeatures=%{fontfeatures}\\nnamelang=%{namelang}\\n" +
        "prgname=%{prgname}\\npostscriptname=%{postscriptname}\\nfonthashint=%{fonthashint}\\n" +
        "order=%{order}\\n"

    const val hexRadix = 16

    const val cacheFileName = "texfonts.pb.gz"
}
