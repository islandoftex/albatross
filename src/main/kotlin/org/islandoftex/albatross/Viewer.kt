// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.albatross

import com.github.ajalt.mordant.animation.progressAnimation
import com.github.ajalt.mordant.rendering.AnsiLevel
import com.github.ajalt.mordant.rendering.BorderType
import com.github.ajalt.mordant.rendering.OverflowWrap
import com.github.ajalt.mordant.rendering.TextAlign
import com.github.ajalt.mordant.rendering.Whitespace
import com.github.ajalt.mordant.table.Borders
import com.github.ajalt.mordant.table.ColumnWidth
import com.github.ajalt.mordant.table.table
import com.github.ajalt.mordant.terminal.Terminal
import com.github.ajalt.mordant.widgets.Spinner

data class Configuration(
    val showStyles: Boolean,
    val detailed: Boolean,
    val matchAny: Boolean,
    val ansiLevel: AnsiLevel,
    val borderType: AlbatrossBorder,
    val includeTeXFonts: Boolean,
    val clearCache: Boolean
)

/**
 * Maps [AlbatrossBorder] to [BorderType].
 * @return [BorderType] or null from [AlbatrossBorder].
 */
private fun AlbatrossBorder.toBorderTypeOrNull() = when (this) {
    AlbatrossBorder.ASCII -> BorderType.ASCII
    AlbatrossBorder.BLANK -> BorderType.BLANK
    AlbatrossBorder.DOUBLE -> BorderType.DOUBLE
    AlbatrossBorder.HEAVY -> BorderType.HEAVY
    AlbatrossBorder.ROUNDED -> BorderType.ROUNDED
    AlbatrossBorder.SQUARE -> BorderType.SQUARE
    AlbatrossBorder.NONE -> null
}

class Viewer(
    private val configuration: Configuration,
    private val glyphs: List<Glyph>
) {
    private val terminal = Terminal(configuration.ansiLevel)
    private val progressOverall = terminal.progressAnimation {
        text("")
        spinner(Spinner.Dots(style = Settings.cliProgressSpinnerStyle))
        text(Settings.cliProgressTextStyle("Please wait while we fetch the fonts."))
    }
    private val progressTeXFonts = terminal.progressAnimation {
        text("")
        spinner(Spinner.Dots(style = Settings.cliProgressSpinnerStyle))
        text(Settings.cliProgressTextStyle("Please wait while we map the TeX fonts."))
    }

    /**
     * Views the query results.
     */
    fun view() {
        terminal.cursor.hide(showOnExit = true)
        when {
            !FontConfig.isAvailable -> viewFontConfigNotFound()
            configuration.includeTeXFonts && !KPSEWhich.isAvailable -> viewKPSEWhichNotFound()
            else -> {
                if (configuration.clearCache) { Cache.clearCache() }
                val additionalTeXFonts = if (configuration.includeTeXFonts) {
                    progressTeXFonts.start()
                    val queries = Cache.getTeXFonts()
                    progressTeXFonts.stop()
                    progressTeXFonts.clear()
                    queries
                } else {
                    emptyList()
                }
                progressOverall.start()
                val map = glyphs.associate {
                    setOf(it) to
                        it.codepoints.map {
                                g ->
                            FontConfig.getFonts(g).toSet() +
                                FontConfig.filterFonts(additionalTeXFonts, g.toInt(Settings.hexRadix)).toSet()
                        }.reduce { a, b -> a.intersect(b) }.toList().sortedBy { g -> g.name }
                }.let {
                    if (configuration.matchAny) {
                        it
                    } else {
                        mapOf(
                            it.keys.flatten().toSet() to it.values.map {
                                    g ->
                                g.toSet()
                            }.reduce { a, b -> a.intersect(b) }.toList().sortedBy { g -> g.name }
                        )
                    }
                }

                progressOverall.stop()
                progressOverall.clear()

                map.forEach { (glyphs, fonts) ->
                    when (fonts.isEmpty()) {
                        true -> viewNoEntries(glyphs)
                        false -> viewEntries(glyphs, fonts)
                    }
                }
            }
        }
    }

    /**
     * Shows a layout in which there are no
     * actual font entries for the provided
     * set of [glyphs].
     */
    private fun viewNoEntries(glyphs: Set<Glyph>) {
        terminal.println(
            if (configuration.borderType != AlbatrossBorder.NONE) {
                table {
                    captionTop(Settings.cliTextStyle(createCaption(glyphs, configuration.detailed)))
                    borderType = configuration.borderType.toBorderTypeOrNull() ?: BorderType.SQUARE
                    borderStyle = Settings.borderColour
                    overflowWrap = OverflowWrap.BREAK_WORD
                    column(0) {
                        width = ColumnWidth.Expand(Settings.noEntriesColumnWidthPercentage)
                        align = TextAlign.CENTER
                        style = Settings.cliTextStyle
                        overflowWrap = OverflowWrap.BREAK_WORD
                        whitespace = Whitespace.PRE_WRAP
                    }
                    body {
                        row("No fonts with support for these Unicode code points were found.")
                    }
                }
            } else {
                "No fonts with support for these Unicode code points were found."
            }
        )
    }

    /**
     * Creates a [String] caption based on the
     * set of [glyphs] and whether the output
     * should be [detailed].
     * @return a [String] caption.
     */
    private fun createCaption(glyphs: Set<Glyph>, detailed: Boolean) = if (glyphs.size == 1) {
        "Unicode glyph with code points ${glyphs.first().codepoints}" +
            if (detailed) { ",\ndetailed, " } else { "\n" } + "mapping to ${listOf(glyphs.first().string)} " +
            if (configuration.matchAny) "(OR search)" else "(AND search)"
    } else {
        "Unicode glyphs with code points ${
        glyphs.joinToString(separator = ", ")
        { it.codepoints.toString() }
        }" + if (detailed) { ",\ndetailed, " } else { "\n" } + "mapping to ${
        glyphs.map { it.string }} " + if (configuration.matchAny) "(OR search)" else "(AND search)"
    }

    /**
     * Views font entries based on the provided
     * [glyphs] and the list of [fonts].
     */
    private fun viewEntries(glyphs: Set<Glyph>, fonts: List<FontData>) {
        val entries = fonts.groupBy { it.name }

        if (!configuration.detailed) {
            if (configuration.borderType != AlbatrossBorder.NONE) {
                terminal.println(
                    table {
                        captionTop(Settings.cliTitleStyle(createCaption(glyphs, configuration.detailed)))
                        overflowWrap = OverflowWrap.BREAK_WORD
                        borderType = configuration.borderType.toBorderTypeOrNull() ?: BorderType.SQUARE
                        borderStyle = Settings.borderColour
                        tableBorders = Borders.ALL
                        column(0) {
                            width =
                                ColumnWidth.Expand(
                                    if (configuration.showStyles) {
                                        Settings.fontNameColumnWidthPercentage
                                    } else {
                                        Settings.fontNameColumnFullWidthPercentage
                                    }
                                )
                            overflowWrap = OverflowWrap.BREAK_WORD
                            whitespace = Whitespace.PRE_WRAP
                        }
                        if (configuration.showStyles) {
                            column(1) {
                                width = ColumnWidth.Expand(Settings.fontStylesColumnWidthPercentage)
                                overflowWrap = OverflowWrap.BREAK_WORD
                                whitespace = Whitespace.PRE_WRAP
                            }
                        }
                        header {
                            if (configuration.showStyles) {
                                row(Settings.cliHeaderStyle("Font name"), Settings.cliHeaderStyle("Available styles"))
                            } else {
                                row(Settings.cliHeaderStyle("Font name"))
                            }
                        }
                        body {
                            style = Settings.cliTextStyle
                            cellBorders = if (configuration.showStyles) Borders.ALL else Borders.NONE
                            if (configuration.showStyles) {
                                entries.forEach { (name, font) ->
                                    row(name, font.flatMap { it.styles }.distinct().joinToString(", "))
                                }
                            } else {
                                entries.keys.forEach { row(it) }
                            }
                        }
                    }
                )
            } else {
                if (configuration.showStyles) {
                    entries.forEach { (name, font) ->
                        terminal.println("Name: $name")
                        terminal.println("Styles: ${font.flatMap { it.styles }.distinct().joinToString(", ")}")
                        terminal.println()
                    }
                } else {
                    entries.keys.forEach { terminal.println(it) }
                }
            }
        } else {
            if (configuration.borderType != AlbatrossBorder.NONE) {
                entries.forEach {
                    terminal.println(
                        table {
                            captionTop(Settings.cliTitleStyle(createCaption(glyphs, configuration.detailed)))
                            borderType = configuration.borderType.toBorderTypeOrNull() ?: BorderType.SQUARE
                            column(0) {
                                width = ColumnWidth.Expand(Settings.detailsKeyColumnWidthPercentage)
                                overflowWrap = OverflowWrap.BREAK_WORD
                                whitespace = Whitespace.PRE_WRAP
                            }
                            column(1) {
                                width = ColumnWidth.Expand(Settings.detailsValueColumnWidthPercentage)
                                overflowWrap = OverflowWrap.BREAK_WORD
                                whitespace = Whitespace.PRE_WRAP
                            }
                            body {
                                row(Settings.cliHeaderStyle("Name"), Settings.cliTextStyle(it.key))
                                row(
                                    Settings.cliHeaderStyle("Type"),
                                    Settings.cliTextStyle(it.value.map { it.format }.distinct().joinToString(", "))
                                )
                                row(
                                    Settings.cliHeaderStyle("Files"),
                                    table {
                                        tableBorders = Borders.NONE
                                        borderType = configuration.borderType.toBorderTypeOrNull() ?: BorderType.SQUARE
                                        column(0) {
                                            width = ColumnWidth.Expand(Settings.detailsPathsColumnWidthPercentage)
                                            overflowWrap = OverflowWrap.BREAK_WORD
                                            whitespace = Whitespace.PRE_WRAP
                                        }
                                        body {
                                            it.value.forEach { row(Settings.cliPathStyle(it.path.toString())) }
                                        }
                                    }
                                )
                                if (configuration.showStyles) {
                                    row(
                                        Settings.cliHeaderStyle("Styles"),
                                        Settings.cliTextStyle(
                                            it.value.map { it.styles }.flatten().distinct().joinToString(", ")
                                        )
                                    )
                                }
                            }
                        }
                    )
                }
            } else {
                entries.forEach {
                    terminal.println("Name: ${it.key}")
                    terminal.println("Type: ${it.value.map { p -> p.format }.distinct().joinToString(", ")}")
                    if (configuration.showStyles) {
                        terminal.println("Styles: ${it.value.flatMap { p -> p.styles }.distinct().joinToString(", ")}")
                    }
                    terminal.println("Files:")
                    it.value.forEach { p -> terminal.println("- ${p.path}") }
                    terminal.println()
                }
            }
        }
    }

    /**
     * Views a message in case the FontConfig
     * tools are not available in the system.
     */
    private fun viewFontConfigNotFound() {
        terminal.println(
            if (configuration.borderType != AlbatrossBorder.NONE) {
                table {
                    captionTop(Settings.cliTitleStyle("Attention!"))
                    borderType = configuration.borderType.toBorderTypeOrNull() ?: BorderType.SQUARE
                    column(0) {
                        width = ColumnWidth.Expand(Settings.notFoundColumnWidthPercentage)
                        align = TextAlign.CENTER
                        style = Settings.cliTextStyle
                        overflowWrap = OverflowWrap.BREAK_WORD
                        whitespace = Whitespace.PRE_WRAP
                    }
                    body {
                        row("This program requires FontConfig tools to be available in your system path.")
                    }
                }
            } else {
                "Attention: This program requires FontConfig tools to be available in your system path."
            }
        )
    }

    /**
     * Views a message in case KPSEWhich
     * is not available in the system.
     */
    private fun viewKPSEWhichNotFound() {
        terminal.println(
            if (configuration.borderType != AlbatrossBorder.NONE) {
                table {
                    captionTop(Settings.cliTitleStyle("Attention!"))
                    borderType = configuration.borderType.toBorderTypeOrNull() ?: BorderType.SQUARE
                    column(0) {
                        width = ColumnWidth.Expand(Settings.notFoundColumnWidthPercentage)
                        align = TextAlign.CENTER
                        style = Settings.cliTextStyle
                        overflowWrap = OverflowWrap.BREAK_WORD
                        whitespace = Whitespace.PRE_WRAP
                    }
                    body {
                        row(
                            "To include fonts from the TeX tree, this program\nrequires " +
                                "KPSEWhich to be available in your system path."
                        )
                    }
                }
            } else {
                "Attention: To include fonts from the TeX tree, this program\n" +
                    "requires KPSEWhich to be available in your system path."
            }
        )
    }
}
